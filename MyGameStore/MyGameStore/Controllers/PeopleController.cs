﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyGameStore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PeopleController : ControllerBase
    {
        private static List<Person> _people;

        static PeopleController()
        {
            _people = new List<Person>
            {
                new Person() { Id = 1, FirstName = "Mathias", LastName = "Maes" },
                new Person() { Id = 2, FirstName = "Cato", LastName = ":(" }
            };
        }

        [HttpGet]
        public IActionResult GetPeople()
        {
            return Ok(_people);
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult GetPerson(int id)
        {
            var person = _people.FirstOrDefault(x => x.Id == id);

            if (person == null) return NotFound();

            return Ok(person);
        }

        [HttpGet]
        [Route("by/lastname")]
        public IActionResult GetPersonByLastName([FromQuery] string lastName)
        {
            return Ok(_people.FindAll(x => x.LastName == lastName));
        }

        [HttpPut]
        public IActionResult AddPerson([FromBody] Person person)
        {
            person.Id = _people.Max(x => x.Id) + 1;
            _people.Add(person);

            return Created("", person);
        }

        [HttpPatch]
        public IActionResult ChangePerson([FromBody] Person person)
        {
            var index = _people.FindIndex(x => x.Id == person.Id);

            if (index == -1)
                return NotFound();

            _people[index] = person;
            return Ok(person);
        }

        [HttpDelete]
        public IActionResult RemovePerson([FromQuery] int id)
        {
            var apiKey = Request.Headers["X-AccessKey"];
            if (apiKey != "123456789")
                return Unauthorized();

            _people.RemoveAll(x => x.Id == id);
            return NoContent();
        }
    }
}
